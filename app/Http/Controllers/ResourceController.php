<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    private function prepareCurlGetRequest($token, $resource_id){
      $ch = curl_init();
      $params = http_build_query(["token" => $token, "resource_id" => $resource_id]);
      $urlPlusGetParams = "http://localhost:8001?".$params;
      curl_setopt($ch,CURLOPT_URL,$urlPlusGetParams);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      return $ch;
    }

    public function getResourceN(Request $request){
      try {
        $resource_id = $request->input("resource_id");
        $token = $request->input("token");
        if(is_null($token) || is_null($resource_id)){
          return json_encode(["message" => "Failed to load required param."]);
        }
        $ch = $this->prepareCurlGetRequest($token, $resource_id);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        $response->path = $request->path();
        return json_encode($response);
      } catch (\Exception $e) {
        return json_encode(["message" => "Failed to proccess request."]);
      }
    }
}
